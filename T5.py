import matplotlib.pyplot as plt
from pandas import read_csv, Series
import numpy as np
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split

max_erros_validacao = 7
lr = 0.1
max_epocas = 100
porcentagem_teste = 0.2 #Separado manualmente entre os arquivos treino.csv e teste.csv
porcentagem_validacao = 0.1
falha_validacao = 10
func_ativacao='relu'
#Quantidade de dias anteriores usados no treinamento
k = 2

def separa_entrada_saida(residuo):
	X = np.zeros((len(residuo)-k, k))
	Y = np.zeros((1,len(residuo)-k))

	for i in range (k,len(residuo)):
		for j in range (k):
			X[i-k][j] = residuo[i+j-k]
		Y[0][i-k] = residuo[i+j-k+1]

	return X, Y.ravel()

#Valores retirados do repositório
dados_treino = read_csv("treino.csv")
dados_teste = read_csv("teste.csv")


dados_treino.drop('Data', axis=1, inplace=True)
dados_treino.drop("Obitos absolutos", axis=1, inplace=True)
tamanho_treino = len(dados_treino)

dados_teste.drop('Data', axis=1, inplace=True)
dados_teste.drop("Obitos absolutos", axis=1, inplace=True)
tamanho_teste = len(dados_teste)


#Decomposição da entrada em Tendência, Ciclo e Resíduo
decomposicao_treino = seasonal_decompose(dados_treino, period=2, model='additive')

#Plot da entrada decomposta
decomposicao_treino.plot()

tendencia_treino = decomposicao_treino.trend
tendencia_treino.fillna(0, inplace=True)

ciclo_treino = decomposicao_treino.seasonal
ciclo_treino.fillna(0, inplace=True)

residuo_treino = decomposicao_treino.resid
residuo_treino.fillna(0, inplace=True)

decomposicao_teste = seasonal_decompose(dados_teste, period=2, model='additive')

tendencia_teste = decomposicao_teste.trend
tendencia_teste.fillna(0, inplace=True)

ciclo_teste = decomposicao_teste.seasonal
ciclo_teste.fillna(0, inplace=True)

residuo_teste = decomposicao_teste.resid
residuo_teste.fillna(0, inplace=True)

#Separação de entrada e saída
x_treino, y_treino = separa_entrada_saida(residuo_treino)
x_teste, y_teste = separa_entrada_saida(residuo_teste)

#Normalização
scaler = MinMaxScaler(feature_range=(0,1))
scaler.fit(residuo_treino.array.reshape(-1,1))

residuo_treino_normalizado = scaler.transform(residuo_treino.array.reshape(-1,1))
x_treino_normalizado = scaler.transform(x_treino.reshape(-1,1)).reshape(-1,k)
y_treino_normalizado = scaler.transform(y_treino.reshape(-1,1)).ravel()
#Criação do modelo
modelo = MLPRegressor(hidden_layer_sizes=(5,), activation=func_ativacao,verbose=False, learning_rate_init=lr,
	validation_fraction=porcentagem_validacao, max_iter=max_epocas, n_iter_no_change=falha_validacao, random_state=100)

modelo.fit(x_treino_normalizado, y_treino_normalizado)

x_teste_normalizado = scaler.transform(x_teste.reshape(-1,1)).reshape(-1,k)
y_teste_normalizado = scaler.transform(y_teste.reshape(-1,1))

y_pred = modelo.predict(x_teste_normalizado).reshape(-1,1)

#Desfaz a normalização das previsões
y_pred = scaler.inverse_transform(y_pred.reshape(-1,1))

#Eixo X do plot do teste
tempo=[]

#Soma tendência e ciclo_treino
for i in range(tamanho_teste-k):
	y_pred[i] = y_pred[i]+ciclo_teste[i]+tendencia_teste[i]
	y_teste[i] = y_teste[i]+ciclo_teste[i]+tendencia_teste[i]
	tempo.append(int(tamanho_treino + i))

plt.figure("Previsão x Realidade")
plt.scatter(tempo,y_pred.ravel(), label="Previsão", c='red')
plt.plot(tempo,y_teste, label="Curva real")
plt.legend()
plt.show()